
	@extends('layout')

	@section('content')

	<h1>Sign up for a daily report of what's for dinner at the Kosher station in BD</h1>

	<p>Enter your email or phone number or both, depending on how you'd like to receive the report.</p>
	<form class=signup action="signup" method="post">
		{{ csrf_field() }}
		<label>Email:</label>
		<br>
		<input type=email name=email id=email>
		<br>
		<br>
		<label>Phone number: (ex: 5551234567)</label>	
		<br>
		<input type="tel" name="num" id="num">
		<br>
		<br>
		<label>Wireless carrier:</label>
		<br>
		<input type="radio" name="carrier" value=verizon id=verizon> Verizon <br>
		<input type="radio" name="carrier" value=att id=att> AT&amp;T <br>
		<input type="radio" name="carrier" value=sprint id=sprint> Sprint <br>
		<input type="radio" name="carrier" value=tmobile id=tmobile> T-mobile <br>
		<input type="radio" name="carrier" value=metro id=metro> MetroPCS <br>
		<input type="radio" name="carrier" value=virgin id=virgin> Virgin mobile <br>
		<br>
		<input type="submit" name="submit" class="button" value="Sign up">
	</form>

	<br>
	<br>
	<br>

	<p>If you'd like to stop receiving these reports, enter the subscribed email and/or phone number below.</p>
	<form class=unsub action="unsubscribe" method="post">
		{{ csrf_field() }}
		<label>Email:</label>
		<br>
		<input type=email name=email id=email>
		<br>
		<br>
		<label>Phone number: (ex: 5551234567)</label>	
		<br>
		<input type="tel" name="num" id="num">
		<br>
		<br>
		<label>Wireless carrier:</label>
		<br>
		<input type="radio" name="carrier" value=verizon id=verizon> Verizon <br>
		<input type="radio" name="carrier" value=att id=att> AT&amp;T <br>
		<input type="radio" name="carrier" value=sprint id=sprint> Sprint <br>
		<input type="radio" name="carrier" value=tmobile id=tmobile> T-mobile <br>
		<input type="radio" name="carrier" value=metro id=metro> MetroPCS <br>
		<input type="radio" name="carrier" value=virgin id=virgin> Virgin mobile <br>
		<br>
		<input type="submit" name="submit" class="button" value="Unsubscribe">
	</form>

	@stop

