# README #

Using PHP Laravel framework.

95 points total

* Creative portion (20 pts)
* Sign up (15 pts)
    * allow users to sign up by email (8 pts)
    * store their information correctly formatted in a database (7 pts)
* Unsubscribe (10 pts)
    * allow users to remove their email from the list in the database
* Scrape and parse the WUSTL kosher dinner menu (15 pts)
* Send emails to users with menu (10 pts)
* Automate the sending to be scheduled daily with Scheduler(10 pts)
* Site looks good and is intuitive (5 pts)
* Best practices are followed (10 pts)


### Who do I talk to? ###

* Elie Schramm and Elan Miller

Elan Miller and Elie Schramm Creative Project Rubric

-Added in ability for user to sign up either solely with their phone number
or with both phone and email and to correspondingly receive only text messages (twilio)
or texts and email
-Ability for user to unregister with phone number as well
-User gets a sign up email/text when they first register letting them know it was successful

url: https://kosherdinner.000webhostapp.com/