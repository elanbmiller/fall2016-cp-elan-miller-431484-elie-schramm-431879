<!DOCTYPE html>
<html>
<head>
	<title>Kosher Report</title>
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->
	<link rel='stylesheet' type='text/css' href="{{ elixir('css/signupStyle.css') }}" />

	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<script>
		 window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    	]); ?>
	</script>

</head>
<body>

	@yield('content')

</body>
</html>
