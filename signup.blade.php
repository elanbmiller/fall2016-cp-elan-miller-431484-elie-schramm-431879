
@extends('layout')

@section('content')

<p>
<br>

	<?php

	// $mysqli = new mysqli('localhost', 'public', 'public', 'kosherDinner');
 
	// if($mysqli->connect_errno) {
	// 	printf("Connection Failed: %s\n", $mysqli->connect_error);
	// 	exit;
	// }


	$email = (string)$_POST['email'];
	// entered an email
	if (isset($email) && $email != "") {
		
		$email_regex = '/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/';
		if (preg_match($email_regex, $email)){
			// $safe_email = $mysqli->real_escape_string($email);	

			// $stmt = $mysqli->query("insert into recipients (address) values ('$safe_email')");
		    $stmt = DB::table('recipients')->insert(['address' => $email]);


		    if($stmt){
		      echo "Successfully signed up by email!<br>";
		    }
		    else {
		      echo "Failed to sign up your email :(<br>";
		    }	
		}
		else {
			echo "Please enter a valid email.<br>";
		}
		
    }


	// entered a phone number
    if(isset($_POST['num'], $_POST['carrier'])){
		$number = (string)$_POST['num'];
		$carrier = (string)$_POST['carrier'];
		$number_regex = '/^\d{10}$/';

		if (preg_match($number_regex, $number)){
			// $safe_number = $mysqli->real_escape_string($number);

			if ($carrier == "verizon"){
				$full_num = $number . "@vtext.com";
			}	
			elseif ($carrier == "att"){
				$full_num = $number . "@txt.att.net";
			}	
			elseif ($carrier == "sprint"){
				$full_num = $number . "@messaging.sprintpcs.com";
			}	
			elseif ($carrier == "tmobile"){
				$full_num = $number . "@tmomail.net";
			}	
			elseif ($carrier == "metro"){
				$full_num = $number . "@MyMetroPcs.com";
			}	
			elseif ($carrier == "virgin"){
				$full_num = $number . "@vmobl.com";
			}
			else {
				echo "Please enter a valid US phone number -- just 10 digits";
				echo "<br><br><a href='KosherHome'> Click here to go back </a>";
				exit;
			}	
			
			// $stmt = $mysqli->query("insert into recipients (address) values ('$full_num')");
		    $stmt = DB::table('recipients')->insert(['address' => $full_num]);

		    if($stmt){
		      echo "Successfully signed up by phone number!<br>";
		    }
		    else{
		      echo "Failed to sign up your phone number :( <br>";
		    }	
		}
		else {
			echo "Please enter a valid US phone number -- just 10 digits <br>";
		}

    }

    elseif (isset($_POST['num']) && !isset($_POST['carrier'])){
    	echo "Please select your wireless carrier. If you don't see yours listed, please contact the admin. <br>";
    }

    echo "<br><br><a href='KosherHome'> Click here to go back </a>";

    ?>

</p>

@stop