<?php

namespace App\Http\Controllers;


class KosherController extends Controller
{

	public function index() {
	    $recipients = \DB::table('recipients')->get();
	    return view('test', compact('recipients'));
	}

	public function signup() {
		$recipients = \DB::table('recipients')->get();
	    return view('signup', compact('recipients'));

	}

	public function unsub() {
		$recipients = \DB::table('recipients')->get();
	    return view('unsubscribe', compact('recipients'));

	}

	public function send() {
		$recipients = \DB::table('recipients')->get();
	    return view('sender', compact('recipients'));

	}
}
