	
	@extends('layout')

	@section('content')

	<p>
	<br>
	<?php

	// $mysqli = new mysqli('localhost', 'public', 'public', 'kosherDinner');
 
	// if($mysqli->connect_errno) {
	// 	printf("Connection Failed: %s\n", $mysqli->connect_error);
	// 	exit;
	// }

	if (isset($_POST['email']) && $_POST['email'] != "") {
		$email = (string)$_POST['email'];
		// $safe_email = $mysqli->real_escape_string($email);

		// $sql = "DELETE FROM recipients WHERE address='$safe_email'";
    	$stmt = DB::table('recipients')->where('address', '=', $email)->delete();
	 //    $stmt = $mysqli->query($sql);
	     if(!$stmt){
	        echo ("Query Prep Failed 1 \n");

	        exit;
	    }
	    else{
	        echo "Successfully unsubscribed your email. <br>";
	    }
	}

	if(isset($_POST['num'], $_POST['carrier'])){

		$number = (string)$_POST['num'];
		$carrier = (string)$_POST['carrier'];
		$number_regex = '/^\d{10}$/';

		if (preg_match($number_regex, $number)){
			// $safe_number = $mysqli->real_escape_string($number);

			if ($carrier == "verizon"){
				$full_num = $number . "@vtext.com";
			}	
			elseif ($carrier == "att"){
				$full_num = $number . "@txt.att.net";
			}	
			elseif ($carrier == "sprint"){
				$full_num = $number . "@messaging.sprintpcs.com";
			}	
			elseif ($carrier == "tmobile"){
				$full_num = $number . "@tmomail.net";
			}	
			elseif ($carrier == "metro"){
				$full_num = $number . "@MyMetroPcs.com";
			}	
			elseif ($carrier == "virgin"){
				$full_num = $number . "@vmobl.com";
			}
			else {
				echo "Please enter a valid US phone number -- just 10 digits";
				echo "<br><br><a href='KosherHome'> Click here to go back </a>";
				exit;
			}	
	
			// $sql2 = "DELETE FROM recipients WHERE address='$full_num'";
			// $stmt = $mysqli->query($sql2);
			$stmt = DB::table('recipients')->where('address', '=', $full_num)->delete();

		    if(!$stmt){
		        echo ("Query Prep Failed 2 \n");
		        exit;
		    }
		    else{
		        echo "Successfully unsubscribed your number. <br>";
		    }
	    }

	    else {
			echo "Please enter a valid US phone number -- just 10 digits <br>";
		}

	}

	echo "<br><br><a href='KosherHome'> Click here to go back </a>";

	?>

	</p>

	@stop